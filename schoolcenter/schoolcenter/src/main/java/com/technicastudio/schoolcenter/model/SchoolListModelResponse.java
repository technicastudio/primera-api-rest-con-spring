package com.technicastudio.schoolcenter.model;

import java.util.List;

/**
 * The Class SchoolListModelResponse.
 */
public class SchoolListModelResponse {
	
	/** The list. */
	private List<SchoolModelResponse> list;

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<SchoolModelResponse> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(List<SchoolModelResponse> list) {
		this.list = list;
	}

}
