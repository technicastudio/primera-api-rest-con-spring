package com.technicastudio.schoolcenter.repository;

import com.technicastudio.schoolcenter.entity.SchoolListEntityResponse;

/**
 * The Interface SchoolRepository.
 */
public interface SchoolRepository {

	/**
	 * Find all.
	 *
	 * @return the school list entity response
	 */
	public SchoolListEntityResponse findAll();

}
