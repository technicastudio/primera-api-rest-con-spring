package com.technicastudio.schoolcenter.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class SchoolListEntityResponse.
 */
public class SchoolListEntityResponse {
	
	/** The list. */
	@JsonProperty("data")
	private List<SchoolEntityResponse> list;

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public List<SchoolEntityResponse> getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(List<SchoolEntityResponse> list) {
		this.list = list;
	}

	
}
