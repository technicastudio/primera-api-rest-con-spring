package com.technicastudio.schoolcenter.service;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.technicastudio.schoolcenter.entity.SchoolListEntityResponse;
import com.technicastudio.schoolcenter.mapper.SchoolMapper;
import com.technicastudio.schoolcenter.model.SchoolListModelResponse;
import com.technicastudio.schoolcenter.repository.SchoolRepository;

/**
 * The Class SchoolServiceImpl.
 */
@Service
public class SchoolServiceImpl implements SchoolService {

	/** The repository. */
	@Autowired
	SchoolRepository repository;

	/** The mapper. */
	@Autowired
	SchoolMapper mapper;

	/**
	 * Filter center.
	 *
	 * @param name the name
	 * @param type the type
	 * @param address the address
	 * @return the school list model response
	 */
	public SchoolListModelResponse filterCenter(String name, String type, String address) {
		// get all entity schools
		SchoolListEntityResponse entityResponse = repository.findAll();

		// filter by name, type and address
		if (name != null) {
			entityResponse.setList(entityResponse.getList().stream().filter(school -> school.getName().contains(name))
					.collect(Collectors.toList()));
		}
		
		if (type != null) {
			entityResponse.setList(entityResponse.getList().stream().filter(school -> school.getType().equals(type))
					.collect(Collectors.toList()));
		}
		
		if (address!= null) {
			entityResponse.setList(entityResponse.getList().stream().filter(school -> school.getAddress().equals(address))
					.collect(Collectors.toList()));	
		}
		

		// convert entity to model response
		SchoolListModelResponse response = mapper.createSchoolListModelResponse(entityResponse);

		return response;
	}

}
