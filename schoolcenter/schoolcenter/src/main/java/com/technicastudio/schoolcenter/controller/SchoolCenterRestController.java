package com.technicastudio.schoolcenter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.technicastudio.schoolcenter.model.SchoolListModelResponse;
import com.technicastudio.schoolcenter.service.SchoolService;

/**
 * The Class SchoolCenterRestController.
 */
@RestController
@RequestMapping("school")
public class SchoolCenterRestController {
	
	/** The service. */
	@Autowired
	SchoolService service;

	/**
	 * List.
	 *
	 * @param name the name
	 * @param type the type
	 * @param adress the adress
	 * @return the response entity
	 */
	@GetMapping(value = "/center")
    public ResponseEntity<SchoolListModelResponse> list(@RequestParam(value = "name", required = false) String name,
    		@RequestParam(value = "type", required = false) String type, @RequestParam(value = "adress", required = false) String adress) {
		
 		SchoolListModelResponse response = service.filterCenter(name, type, adress);
        
		return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
